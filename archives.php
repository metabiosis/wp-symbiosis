<?php
/*
Template Name: Archives
*/
?>

<?php get_header(); ?>

<div id="content">
<div class="post">
<h3>Archives by Month:</h3>
  <ul>
    <?php wp_get_archives('type=monthly'); ?>
  </ul>

<h3>Archives by Subject:</h3>
  <ul>
     <?php wp_list_cats('sort_column=name&optioncount=1'); ?>
  </ul>

<h3>Last 30:</h3>
 <ul>
		<?php wp_get_archives('type=postbypost&limit=30'); ?>
	</ul>
</div>
</div>	
<?php get_sidebar(); ?>
<?php get_footer(); ?>
