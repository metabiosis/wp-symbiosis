<div id=metasidebar>
<div id="sidebar-2" class="sidebar">
<ul>
 <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(2) ) : else : ?>
	<li>
	<h2><?php _e('About'); ?></h2>
	<p><?php bloginfo('description'); ?></p>
	</li>
	<?php if (function_exists('plogger_press_sidebar')) { ?> 
	<li>
		<h2><a href="<?php bloginfo('url'); ?>/photos">Photos</a></h2>
		<ul>
		  <?php plogger_press_sidebar(6);?>
		</ul>
	</li>
	<?php } ?>
	<li>
	<h2><?php _e('Meta'); ?></h2>
		<ul>
			<li><a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Syndicate this site using RSS 2.0'); ?>"><?php _e('Entries <abbr title="Really Simple Syndication">RSS</abbr>'); ?></a></li>
			<li><a href="<?php bloginfo('comments_rss2_url'); ?>" title="<?php _e('The latest comments to all posts in RSS'); ?>"><?php _e('Comments <abbr title="Really Simple Syndication">RSS</abbr>'); ?></a></li>
			<li><a href="http://wordpress.org" title="<?php _e('Powered by Wordpress, state-of-the-art semantic personal publishing platform.'); ?>">Wordpress</a></li>
			<?php wp_meta(); ?>
		</ul>
	</li>
	
	<?php endif; ?>
</ul>
</div>
<div id="sidebar-1" class="sidebar">
<ul>
	 <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>

	<?php wp_list_pages('title_li=<h2>' . __('Pages') . '</h2>' ); ?>
	
	<li>
		<h2><?php _e('Archives'); ?></h2>
		<ul>
		<?php wp_get_archives('type=monthly'); ?>
		</ul>
	</li>
	
	<li>
		<h2><?php _e('Categories'); ?></h2>
		<ul>
		<?php wp_list_cats('sort_column=name&optioncount=1'); ?> 
		</ul>
	</li>
	<?php get_links_list(); ?>
      <li>
		<h2><?php _e('Search'); ?></h2>
		<?php include (TEMPLATEPATH . '/searchform.php'); ?>
	</li>
	<?php endif; ?>
</ul>
</div>
</div>
