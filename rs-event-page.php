<?php
/*
Template Name: RS Event Page
*/
?>

<?php get_header(); ?>

	<div id="content" class="narrowcolumn">
    
    <h3><?php _e('Upcoming events:'); ?></h3>
    
    <ul>
      
      <?php rs_event_list(); ?>
      
    </ul>
    
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>