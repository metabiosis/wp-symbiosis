<?php get_header(); ?>

	<div id="content">
				
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<div class="navigation">
			<div class="alignleft"><?php previous_post_link('&laquo; %link') ?></div>
			<div class="alignright"><?php next_post_link('%link &raquo;') ?></div>
		</div>
	
		<div class="post" id="post-<?php the_ID(); ?>">
			<h2><a href="<?php echo get_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a></h2>
			<p class="postmetadata">
			<?php the_time('l, F jS, Y') ?> - <b><?php the_author() ?></b>
			<br />
			<?php comments_rss_link('RSS 2.0'); ?> 
                        <?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
                        // Both Comments and Pings are open ?>
                        - <a href="<?php trackback_url(true); ?>" rel="trackback">trackback</a>
                        <?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
                        // Only Pings are Open ?>
                        - <a href="<?php trackback_url(true); ?>" rel="trackback">trackback</a>
                        <?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
                        // Comments are open, Pings are not ?>
                        <?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
                        // Neither Comments, nor Pings are open ?>
                        <?php } edit_post_link('( edit )','',''); ?></p>

				<div class="entrytext">
				<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
				
				<?php link_pages('<p><strong>Pages:</strong> ', '</p>', 'number'); ?>
			
		</div>
		
	<?php comments_template(); ?>
	</div>
	<?php endwhile; else: ?>
		<p>Sorry, no posts matched your criteria.</p>
	
<?php endif; ?>
	
	</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
