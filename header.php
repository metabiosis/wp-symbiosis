<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>

<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<link href='/favicon.ico' rel='icon'/>
<link href='/favicon.ico' rel='shortcut icon'/>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<?php wp_head(); ?>

<!-- init the slide sidebar plugin -->
<?php if (function_exists('SLIDE_init')) SLIDE_init(true); ?>

<?php
  if (function_exists('SLIDE_add')) {
      SLIDE_add(
	      'page', // container id
	      'metasidebar', // slider id
	      'slide the sidebar 1', // short description
	      'SLIDE_sidebar', // cookie name
	       0.5, // sliding speed
	       40, // update interval
	      'footer' // breaker id
      );	
  }
?>

</head>
<body>

<div id="page">
<div id="header">
		<a href="<?php echo get_settings('home'); ?>/"><h1><span><?php bloginfo('name'); ?></span></h1></a>
		<div class="description"><?php bloginfo('description'); ?></div>
</div>
